## 0.1.4

- Added `scan-object-acl` command
- Added `--only` and `--except` options
- Added `--log-bucket` and `--log-prefix` options to `scan` command
- Added `--log-prefix` option to `enable-logging` command

## 0.1.3

- Fixed policy check
- Added `list-policy` command
- Added `reset-object-acl` command
- Added support for wildcards
- Added support for customer-provided encryption key
- Added `--version` option
- Parallelize encryption

## 0.1.2

- Fixed issue with packaging

## 0.1.1

- Fixed json error
- Better message for missing credentials

## 0.1.0

- First release
